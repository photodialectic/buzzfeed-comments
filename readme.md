#Summary
The application uses require, jQuery, moment and twig. Moment is used to format time. Twig is the templating language.

##Local environment
There are two challenges to this application using a local file system environment. This first is cross-origin resource sharing with regards to calling the API. The second is particular to Chrome where it doesn't allow Ajax calls to file:// protocols. The solution for both in Chrome is to enable both via flags. For example, in OS X you can open Chrome using the following
```
open -a "Google Chrome" --args --allow-file-access --disable-web-security
```
For convenience, OS X users can run bootstrap.sh which will launch the app in Chrome passing the proper flags for local development.

###Twig
I have made a slight modification to the twig source code to support local development. In order to load templates via AJAX I modified line 723 to accept xmlhttp.status == 0 since files loading locally don't have a http status.

###CORS workaround
If you're not interested in disabling web security to counter the CORS issue, you can put the application into a dev mode where it will load the resources from the resources directory. Set the following in comments.js

```
dev : true,
```