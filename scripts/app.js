require.config({
    config: {
        moment: {
            noGlobal: true
        }
    },
    paths: {
        'jquery' : '../lib/jquery-1.11.1.min',
        'twig'   : '../lib/twig-0.7.2',
        'moment' : '../lib/moment'
    }
});
define(['comments', 'render', 'jquery', 'moment'], function (comments, render, jquery, moment) {

    var bfComments = {
        loadComments: function() {
            if('undefined' === typeof bfCommentConfig)
            {
                console.error('bfCommentConfig not defined and is a required dependency');
                return;
            }
            var id   = bfCommentConfig.content_id,
                page = bfCommentConfig.next_page;
            comments.getComments(id, function(data){bfComments.handleRawResponse(data);}, page);
            ++bfCommentConfig.next_page;
        },
        handleRawResponse: function(data){
            if('undefined' === typeof data.paging.next)
            {
                jquery('.load-more-button').prop('disabled', true);
                jquery('.load-more-button').text('No more responses...');
            }
            this.renderComment(data.comments);
        },
        renderComment: function(comments){
            if('undefined' === typeof bfCommentConfig)
            {
                console.error('bfCommentConfig not defined and is a required dependency');
                return;
            }
            for(index in comments) {
                data               = comments[index];
                data.article_title = bfCommentConfig.title;
                data.friendly_date = moment(data.f_raw);

                if('undefined' !== typeof comments[index].parent_id) {
                    jquery('*[data-comment-id="'+ comments[index].parent_id +'"]').append(render.comment(data));

                } else {
                    jquery('.comments-block').append(render.comment(data));
                }

                //recursion to render children
                if('undefined' !== typeof comments[index].children) {
                    this.renderComment(comments[index].children);
                }
            }
        }
    }

    //bind paginator
    jquery('.load-more-button').bind('click', function(){
        bfComments.loadComments();
    });

    return bfComments;
});