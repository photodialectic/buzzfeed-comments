define(['twig'], function (twig) {
    twig = twig.twig;

    var contribution = twig({
        id: 'contribution',
        href: 'templates/contribution.twig',
    });

    var reaction = twig({
        id: 'reaction',
        href: 'templates/reaction.twig'
    });

    var render = {
        comment: function(data) {
            switch(data.comment_type) {
                case 'reaction':
                    return twig({ref: 'reaction'}).render(data);
                    break;
                case 'contribution':
                default:
                    return twig({ref: 'contribution'}).render(data);
                    break;
            }
        },
    }
    return render;
});