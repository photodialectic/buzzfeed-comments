define(['jquery'], function (jquery) {
    var comments = {
        dev           : false,
        endpointURL   : 'http://www.buzzfeed.com/api/v1/comments/',
        getComments : function(contentID, callback, page){
            endpoint  = this.dev ? 'resources/' : this.endpointURL;
            page      = typeof page !== 'undefined' ? page : 1;
            queryGlue = this.dev ? '_' : '?';
            jquery.getJSON(endpoint + contentID + queryGlue + 'p=' + page, function(data) {callback(data)})
                  .fail(function( jqxhr, textStatus, error){
                    console.error('Comment fetch failed with error message: ' + error);
                });
        },
    }
    return comments;
});